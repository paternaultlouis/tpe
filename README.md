⚠️ *Attention !*  Avec la réforme du bac 2021, les TPE disparaîssent. Ce site ne sera donc plus mis à jour, et deviendra bientôt obsolète.

Ce dépôt contient divers documents relatifs à l'encadrement des [travaux personnels encadrés](http://eduscol.education.fr/cid47789/tpe.html) (dans l'enseignement public français), en mathématiques, mais aussi dans un cadre plus général.

Les documents de ce dépôt dont je suis l'auteur sont publiés sous license [Creative Commons by-sa 4.0](https://creativecommons.org/licenses/by-sa/4.0/deed.fr), qui en permet, entre autre, la modification et la réutilisation en classe, gratuitement ([j'explique ici](http://ababsurdo.fr/blog/pourquoi_publier_sous_licence_libre/) mon choix de diffusion libre).

Pour me contacter, voir [la page dédiée](http://ababsurdo.fr/apropos/).
